﻿using System;

namespace Ghiceste_numarul
{
    class Program
    {
        static void Main(string[] args)
        {
            Random rand = new Random();
            int numarAleator = rand.Next(0, 100);

            Console.WriteLine("Numarul introdus este: ");
            int nr = int.Parse(Console.ReadLine());

            int i = 1;
            

            while (nr != numarAleator)
            {
                if (Math.Abs(nr - numarAleator) <= 3)
                    Console.WriteLine("Foarte fierbinte");
                else if (Math.Abs(nr - numarAleator) <= 5)
                         Console.WriteLine("Fierbinte");
                else if (Math.Abs(nr - numarAleator) <= 10)
                         Console.WriteLine("Cald");
                else if (Math.Abs(nr - numarAleator) <= 20)
                         Console.WriteLine("Caldut");
                else if (Math.Abs(nr - numarAleator) <= 50)
                         Console.WriteLine("Rece");
                else if (Math.Abs(nr - numarAleator) > 50)
                        Console.WriteLine("Foarte rece");

                nr = int.Parse(Console.ReadLine());
                i++;

            }
            Console.WriteLine("Felicitari, ati ghicit numarul din " + i + " incercari");
            Console.ReadKey();
        }
    }
}
